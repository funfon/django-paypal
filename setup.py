import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

lib_folder = os.path.dirname(os.path.realpath(__file__))
req_path = os.path.join(lib_folder, 'requirements.txt')
install_req = []
if os.path.isfile(req_path):
    with open(req_path) as file:
        install_req = file.read().splitlines()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-paypal',
    version='0.0.1',
    packages=find_packages(),
    install_requires=install_req,
    include_package_data=True,
    description='Django_paypal package.',
    long_description=README,
    long_description_content_type='text/markdown',
    author='Ilia Levikov',
    author_email='fonych1@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'License :: OSI Approved :: MIT License',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
    ],
)
