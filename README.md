# PayPal

### Prerequisites

```text
python 3.7
django
```

### Installation

Add git url to requirement.txt file
```bash
git+https://funfon@bitbucket.org/funfon/django-paypal.git
```

1. Add 'u_paypal' tp your INSTALLED_APPS setting like this
```bash
INSTALLED_APPS = [
    ...
    'u_paypal',
]
```

2. Include the package urls to your project urls.py like this
```bash
path('url/', include('u_paypal.urls'))
```

3. Add PayPal credentials to django setting.py file 
```text
PAYPAL_CLIENT_ID = 'VXr9QhrwQwRXadndBk1AQ2l1tevTgC81qR8wC4EU5ERieMY2NM1T7mVuALe2BO6yfykYHGfM2UQjHfGC'
PAYPAL_CLIENT_SECRET = 'EB2efTQeLiPASSmPiAY5Y1K7BJ4CRuwN2ZLXqJKO5x53AcAafQAi_Blq4uPNrSFif-gfmqWG9XBjFfwWc'
PAYPAL_REDIRECT = 'https://url.to.frontend'
PAYPAL_MODE = 'sandbox/live'
```

4. Run `python manage.py migrate` to create models.


### Authors

[Ilia Levikov](https://www.instagram.com/i.levikov4/ "Instagram") 
