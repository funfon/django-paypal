import json
import logging

from django.conf import settings
from django.http.response import HttpResponseRedirect
import paypalrestsdk
from rest_framework.views import APIView
from rest_framework import status, permissions, viewsets
from rest_framework.response import Response
import requests

from u_paypal.utils.views import action, get_redirect_link_for_payment
from u_paypal.serializers.account import AccountSerializer
from u_paypal.models.account import Account

logger = logging.getLogger('app')


class BalanceView(viewsets.ViewSet):
    """
    return balance for user
    """

    def list(self, request):
        """
        :return: queryset of Account serializer or 400
        """
        if Account.objects.filter(user_identifier=request.user.pk).exists():
            qs = Account.objects.get(user_identifier=request.user.pk)
            ser = AccountSerializer(qs)

            return Response(status=status.HTTP_200_OK, data=ser.data)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class PaymentView(APIView):
    """
    handler for payment
    """

    def post(self, request, *args, **kwargs):
        """
        handle POST request from user
        :return: redirect to PayPal
        """
        amount = request.data.get('amount', None)
        host = settings.PAYPAL_REDIRECT

        logger.debug(f'amount: {amount}, user_id: {request.user.pk}')

        if amount:
            return Response(
                status=status.HTTP_200_OK,
                data={
                    'url': get_redirect_link_for_payment(request.user.pk, amount, host)
                },
            )

        else:
            return Response(
                status=status.HTTP_400_BAD_REQUEST,
                data={'error': 'No amount of payment'},
            )


class SuccessView(APIView):
    """
    handler for success response from PayPal
    """

    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):

        payment_id = request.GET.get('paymentId', None)
        token = request.GET.get('token', None)
        payer_id = request.GET.get('PayerId', None)

        data = {'payment_id': payment_id, 'token': token, 'payer_id': payer_id}

        r = requests.post(
            url=f'{settings.PAYPAL_REDIRECT}api/paypal/success/', data=data
        )

        logger.debug(r.text)

        response = (
            f"?pay_status=success&amount={json.loads(r.text).get('amount', None)}"
        )

        return HttpResponseRedirect(redirect_to=f'{settings.PAYPAL_REDIRECT}{response}')

    def post(self, request, *args, **kwargs):
        """
         handle POST request from user
        :return: redirect to PayPal
        """
        pid = request.POST.get('payment_id', None)

        if pid:
            amount = action(pid, 'success')
            data = {'amount': str(amount)}
            st = status.HTTP_201_CREATED

        else:
            data = {'error': 'No payment data'}
            st = status.HTTP_400_BAD_REQUEST

        return Response(status=st, data=data)


class FailView(APIView):
    """
    handler for fail response from PayPal
    """

    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        pid = request.GET.get('paymentId', None)

        action(pid, 'fail')

        response = '?pay_status=error'

        return HttpResponseRedirect(redirect_to=f'{settings.PAYPAL_REDIRECT}{response}')
