from django.apps import AppConfig


class PayPalAppConfig(AppConfig):
    name = 'u_paypal'
    verbose_name = "PayPal"

    def ready(self):
        pass
