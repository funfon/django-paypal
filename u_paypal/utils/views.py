import uuid
import logging
import paypalrestsdk
import decimal
from decimal import Decimal

from django.conf import settings

from u_paypal.models.account import Account, Payment


logger = logging.getLogger('app')


def action(pid, status):
    """
    :param pid: str, required, pid of transaction
    :param status: str, required, success of fail
    :return: amount of transaction
    """

    payment_info = get_payment_info(pid)

    if (
        payment_info.get('amount', None)
        and payment_info.get('invoice_number', None)
        and payment_info.get('user_guid', None)
    ):

        create_account(
            payment_info.get('user_guid'),
            payment_info.get('amount'),
            payment_info.get('invoice_number'),
            status,
        )

    else:

        logger.debug(
            f"can not create account.\nuser_guid: {payment_info.get('user_guid', None)}"
            f"\namount: {payment_info.get('amount', None)}"
            f"\ninvoice: {payment_info.get('invoice_number', None)}"
        )

    return payment_info.get('amount', None)


def get_payment_info(pid):
    """
    :param pid: str, required
    :return: dict with payment information
    """
    payment_info = {}
    try:
        payment = paypalrestsdk.Payment.find(pid)
        payment_info['user_guid'] = payment.transactions[0].description
        payment_info['invoice_number'] = payment.transactions[0].invoice_number
        payment_info['amount'] = payment.transactions[0].amount.total

        logger.debug(
            f"amount: {payment_info.get('amount')}, "
            f"inv_num: {payment_info.get('invoice_number')},"
            f" user: {payment_info.get('user_guid')}"
        )

    except:

        logger.debug('can not get guid, invoice number or amount')

    return payment_info


def create_account(guid, amount, invoice_number, status):
    """
    Creating account
    :param guid: uuid, required, user's guid
    :param amount: decimal, required
    :param invoice_number: uuid, required
    :param status: str, required
    """
    account = get_an_account(guid)

    if validate_amount(amount):

        account.amount += Decimal(amount)
        account.save()

        logger.debug(f'account {account.user_identifier} saved')

    else:
        logger.error(f'amount of transaction is less than zero: {amount}')

        raise ValueError(f'amount of transaction is less than zero: {amount}')

    create_payment(account, amount, invoice_number, status, 'PayPal')


def get_an_account(guid):
    """
    :param guid: uuid, required
    :return: Account
    """
    if Account.objects.filter(user_identifier=guid).exists():
        account = Account.objects.get(user_identifier=guid)

        logger.debug(f'got an existing account {guid}')

    else:
        account = Account()
        account.user_identifier = int(guid)

        logger.debug(f'created new account {guid}')

    return account


def validate_amount(amount):
    """
    Validating amount
    :param amount: decimal, required
    """
    try:
        return True if Decimal(amount) > 0 and -Decimal(amount).as_tuple().exponent <= 2 else False

    except (ValueError, decimal.InvalidOperation):
        logger.error(f'amount of transaction is not a valid number: {amount}')


def create_payment(account, amount, invoice_number, status, payment_method):
    """
    Creating payment
    :param account: Count, required
    :param amount: decimal, required
    :param invoice_number: uuid, required
    :param status: str, required
    :param payment_method: str, required
    """
    payment = Payment()

    payment.invoice_number = invoice_number
    payment.account = account
    payment.amount = amount
    payment.status = status
    payment.payment_method = payment_method
    payment.operation_type = 'account replenishment'

    payment.save()

    logger.debug(f'payment {invoice_number} saved')


def get_redirect_link_for_payment(guid, amount, host):
    """
    :param guid: string, required
    :param host: string, required
    :param amount: decimal, required
    :return: link to PayPal to pay transaction
    """
    paypalrestsdk.configure(
        {
            "mode": settings.PAYPAL_MODE,
            "client_id": settings.PAYPAL_CLIENT_ID,
            "client_secret": settings.PAYPAL_CLIENT_SECRET,
        }
    )

    if validate_amount(amount):
        payment = paypalrestsdk.Payment(get_payment_data(guid, amount, host))

        return create_paypal_payment(payment)
    else:
        return f'not a valid amount: {amount}'


def create_paypal_payment(payment):
    if payment.create():
        for link in payment.links:
            if link.rel == 'approval_url':
                logger.debug(f'returning PayPal link {link.href}')

                return link.href

    else:
        logger.error(payment.error)
        return f'error in payment: {payment.error}'


def get_payment_data(guid, amount, host):
    """
    :param guid: string, required
    :param host: string, required
    :param amount: decimal, required
    :return: payment dictionary for PayPal
    """
    return {
        "intent": "sale",
        "payer": {"payment_method": "paypal"},
        "redirect_urls": {
            "return_url": f"{host}api/paypal/success/",
            "cancel_url": f"{host}api/paypal/fail/",
        },
        "transactions": [
            {
                "item_list": {
                    "items": [
                        {
                            "name": "refill",
                            "sku": "refill",
                            "price": str(Decimal(amount)),
                            "currency": "USD",
                            "quantity": 1,
                        }
                    ]
                },
                "amount": {"total": str(Decimal(amount)), "currency": "USD"},
                "description": guid,
                "invoice_number": str(uuid.uuid4()),
            }
        ],
    }
