from rest_framework import serializers

from u_paypal.models.account import Account, Payment


class AccountSerializer(serializers.ModelSerializer):
    """
    serialize account model
    """
    class Meta:
        model = Account
        fields = ('amount',)


class PaymentSerializer(serializers.ModelSerializer):
    """
    serialize payment model
    """
    class Meta:
        model = Payment
        fields = ('date', 'payment_method', 'amount', 'status', 'operation_type',)
