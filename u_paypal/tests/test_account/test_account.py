import pytest

from rest_framework.test import APIClient

from u_paypal.tests.factories.factory import AccountFactory


@pytest.mark.django_db(transaction=True)
class TestUPayPal(object):

    api_client = APIClient()

    def test_payment_endpoint(self):
        """
        Test if payment endpoint is available
        """

        resp = self.api_client.post('/api/api/paypal/payment/')
        assert resp.status_code == 400

        resp = self.api_client.post('/api/api/paypal/payment/', {'amount': '250'})
        assert resp.status_code == 200

    def test_payment_amount(self):
        """
        Test different types of amount in endpoints
        """
        resp = self.api_client.post('/api/paypal/payment/', {'amount': 'qwerty'})
        assert resp.data.get('url', None) == 'not a valid amount: qwerty'

        resp = self.api_client.post('/api/paypal/payment/', {'amount': '234,4343'})
        assert resp.data.get('url', None) == 'not a valid amount: 234,4343'

        resp = self.api_client.post('/api/paypal/payment/', {'amount': '234.4343'})
        assert resp.data.get('url', None) == 'not a valid amount: 234.4343'

        resp = self.api_client.post('/api/paypal/payment/', {'amount': '234.43'})
        assert resp.status_code == 200

    def test_balance_endpoint(self):
        """
        Test if balance endpoint is available
        """

        resp = self.api_client.get('/api/paypal/balance/')
        assert resp.status_code == 400

        # creating account to get response 200 with data
        AccountFactory(user_identifier=self.user.pk)

        resp = self.api_client.get('/api/paypal/balance/')
        assert resp.status_code == 200

    def test_invoice_fail(self):
        """
        Test if duplicated invoice will fail
        """

        resp = self.api_client.get('/api/paypal/success/', {'paymentId': '1234567890ABCDE', 'token': '1234'})
        assert resp.status_code == 400

