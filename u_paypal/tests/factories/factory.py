import uuid

import factory.fuzzy
from u_paypal.models.account import Account, Payment


class AccountFactory(factory.django.DjangoModelFactory):
    """
    Account test model
    """
    class Meta:
        model = Account

    user_identifier = factory.fuzzy.FuzzyInteger(1, 100)
    amount = factory.fuzzy.FuzzyDecimal(0)


class PaymentFactory(factory.django.DjangoModelFactory):
    """
    Payment test model
    """
    class Meta:
        model = Payment

    account = factory.SubFactory(AccountFactory)
    invoice_number = uuid.uuid4()
    amount = factory.fuzzy.FuzzyDecimal(0)
    # date = factory.fuzzy.BaseFuzzyDateTime(datetime.datetime.now())
    payment_method = factory.fuzzy.FuzzyText(length=5)
    operation_type = factory.fuzzy.FuzzyText(length=6)
    status = factory.fuzzy.FuzzyText(length=7)
