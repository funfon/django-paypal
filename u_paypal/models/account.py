from decimal import Decimal

from django.db import models


class Account(models.Model):
    """
    Account model
    """
    user_identifier = models.IntegerField(verbose_name='user identifier', null=True, blank=True)
    amount = models.DecimalField(verbose_name='total amount of currency on user account',
                                 decimal_places=2,
                                 max_digits=10,
                                 default=Decimal(0))


class Payment(models.Model):
    """
    Payment model
    """
    account = models.ForeignKey(Account, verbose_name='account of payment', null=False, on_delete=models.CASCADE)
    invoice_number = models.UUIDField(verbose_name='unique identifier for payment', null=True, blank=True)
    amount = models.DecimalField(verbose_name='payment amount in USD',
                                 decimal_places=2,
                                 max_digits=10,
                                 blank=True,
                                 null=True)
    date = models.DateTimeField(verbose_name='date of a payment', null=False, auto_now_add=True)
    payment_method = models.CharField(verbose_name='method of payment', blank=True, null=True, max_length=255)
    status = models.CharField(verbose_name='success or fail', blank=True, null=True, max_length=255)
    operation_type = models.CharField(verbose_name='type of operation', blank=True, null=True, max_length=255)
