from django.urls import path, include
from rest_framework import routers

from u_paypal.views.account import SuccessView, FailView, PaymentView, BalanceView

app_name = 'u_paypal'

router = routers.DefaultRouter()

router.register(r'balance', BalanceView, basename='balance')


urlpatterns = [
    path('api/paypal/payment/', PaymentView.as_view(), name='payment'),
    path('api/paypal/success/', SuccessView.as_view(), name='success'),
    path('api/paypal/fail/', FailView.as_view(), name='fail'),
    path('api/paypal/', include(router.urls)),
]

